# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

DESCRIPTION="uid for net-proxy/ciadpi"
ACCT_USER_ID=901
ACCT_USER_GROUPS=( ciadpi )

acct-user_add_deps
