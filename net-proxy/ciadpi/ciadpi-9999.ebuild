# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs systemd tmpfiles git-r3

PACKAGEAUTHOR="hufrea"
PACKAGENAME="byedpi"

EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PACKAGENAME}.git"

if [[ ${PV} = 9999* ]]; then
	KEYWORDS="~amd64 ~ppc64 ~arm64 ~x86 ~arm ~mips"
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="amd64 ppc64 arm64 x86 arm mips"
fi

DESCRIPTION="ciadpi local SOCKS proxy server made to bypass DPI"
HOMEPAGE="https://github.com/${PACKAGEAUTHOR}/${PACKAGENAME}"

LICENSE=""
SLOT="0"

IUSE="openrc systemd"

DEPEND="
	acct-group/${PN}
	acct-user/${PN}
"

RDEPEND="${DEPEND}"

src_install() {
	dobin ${PN}
	dodoc README.md

	if use openrc ; then
		newinitd "${FILESDIR}/${PN}.initd" ${PN}
		newconfd "${FILESDIR}/${PN}.confd" ${PN}
	fi

	if use systemd ; then
		systemd_newunit "${FILESDIR}"/${PN}-0.12.service ${PN}.service
		insinto /etc/${PN}/
		doins "${FILESDIR}/${PN}.conf"
	fi

	dotmpfiles "${FILESDIR}"/${PN}.tmpfiles.conf
}

pkg_postinst() {
	tmpfiles_process ${PN}.tmpfiles.conf
}
