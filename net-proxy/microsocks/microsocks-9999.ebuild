# Copyright 2020-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs systemd tmpfiles git-r3

PACKAGEAUTHOR="rofl0r"
PACKAGENAME="byedpi"

EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"

if [[ ${PV} = 9999* ]]; then
	KEYWORDS="~amd64 ~x86"
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="Multithreaded, small, efficient SOCKS5 server"
HOMEPAGE="https://github.com/${PACKAGEAUTHOR}/${PN}"

LICENSE="MIT"
SLOT="0"

IUSE="openrc systemd"

DEPEND="
	acct-group/${PN}
	acct-user/${PN}
"

RDEPEND="${DEPEND}"

src_prepare() {
	default
	sed -r -e 's:/usr/local:/usr:' -i Makefile || die
}

src_compile() {
	emake CC="$(tc-getCC)"
}

src_install() {
	default

	if use openrc ; then
		newinitd "${FILESDIR}/${PN}.initd" ${PN}
		newconfd "${FILESDIR}/${PN}.confd" ${PN}
	fi

	if use systemd ; then
		systemd_newunit "${FILESDIR}"/${PN}-1.0.4.service ${PN}.service
		insinto /etc/${PN}/
		doins "${FILESDIR}/${PN}.conf"
	fi

	dotmpfiles "${FILESDIR}"/${PN}.tmpfiles.conf
}

pkg_postinst() {
	tmpfiles_process ${PN}.tmpfiles.conf
}
