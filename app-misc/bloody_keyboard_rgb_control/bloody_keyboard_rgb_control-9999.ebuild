# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PACKAGEAUTHOR="apo5tol"

PYTHON_COMPAT=( python3_{10..13} )

inherit distutils-r1

#https://github.com/apo5tol/bloody_keyboard_rgb_control

if [[ "${PV%9999}" != "${PV}" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS="~amd64 ~x86"
else
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="RGB control for A4Tech Bloody keyboard for Linux"
HOMEPAGE="https://github.com/${PACKAGEAUTHOR}/${PN}"
LICENSE="MIT"
SLOT="0"
IUSE=""
RESTRICT="mirror"

DEPEND="
	>=dev-python/pyusb-1.1.0
"

RDEPEND="
"

#src_configure() {
#}

#pkg_postinst() {
#        xdg_desktop_database_update
#        xdg_mimeinfo_database_update
#}

#pkg_postrm() {
#        xdg_desktop_database_update
#        xdg_mimeinfo_database_update
#}
