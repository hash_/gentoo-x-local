# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

inherit cmake-multilib git-r3

DESCRIPTION="Official GTK+ port of KDE's Oxygen widget style"
HOMEPAGE="
		https://store.kde.org/p/1005553/
		https://invent.kde.org/plasma/oxygen-gtk
"

EGIT_REPO_URI="
		https://invent.kde.org/plasma/oxygen-gtk
"

LICENSE="LGPL-2.1"
KEYWORDS=""
SLOT="0"
IUSE="debug doc"

DEPEND="
	dev-libs/dbus-glib[${MULTILIB_USEDEP}]
	dev-libs/glib:2[${MULTILIB_USEDEP}]
	x11-libs/cairo[${MULTILIB_USEDEP}]
	x11-libs/gdk-pixbuf[${MULTILIB_USEDEP}]
	x11-libs/gtk+:2[${MULTILIB_USEDEP}]
	x11-libs/libX11[${MULTILIB_USEDEP}]
	x11-libs/pango[${MULTILIB_USEDEP}]
"
RDEPEND="${DEPEND}
	doc? ( app-doc/doxygen )
"
BDEPEND="virtual/pkgconfig"

PATCHES=(
	"${FILESDIR}/${PN%3}-1.4.6-qtpaths.patch"
	"${FILESDIR}/${PN%3}-1.4.6-tabstyle.patch"
)

DOCS=(AUTHORS README)

multilib_src_configure() {
	if ! multilib_is_native_abi; then
		local mycmakeargs=(
			-DENABLE_DEMO=OFF
		)
	fi
	cmake_src_configure
}

src_install() {
	if use doc; then
		{ cd "${S}" && doxygen Doxyfile; } || die "Generating documentation failed"
		HTML_DOCS=( "${S}/doc/html/" )
	fi

	cmake-multilib_src_install

	cat <<-EOF > 99oxygen-gtk
CONFIG_PROTECT="/usr/share/themes/oxygen-gtk/gtk-2.0"
EOF
	doenvd 99oxygen-gtk
}
