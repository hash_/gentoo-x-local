# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PACKAGEAUTHOR="paolostivanin"

#CMAKE_MAKEFILE_GENERATOR=emake

inherit cmake

if [[ "${PV%9999}" != "${PV}" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="Library written in C for encoding and decoding data using base32 or base64 (RFC-4648)"
HOMEPAGE="https://github.com/${PACKAGEAUTHOR}/${PN}"
LICENSE="GPL-3"
SLOT="0"
IUSE=""

DEPEND="
	>=sys-devel/gcc-6.4.0
	>=dev-util/cmake-3.8.2
"

RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX:PATH=/usr ../${P}
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install
}
