# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PACKAGEAUTHOR="paolostivanin"

if [[ "${PV%9999}" != "${PV}" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

inherit cmake

DESCRIPTION="C library that generates TOTP and HOTP"
HOMEPAGE="https://github.com/${PACKAGEAUTHOR}/${PN}"
LICENSE="Apache-2.0"
SLOT="0"
IUSE=""
RESTRICT="mirror"

DEPEND="
	>=sys-devel/gcc-6.4.0
	>=dev-util/cmake-3.8.2
	>=dev-libs/libbaseencode-1.0.11
"
src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX:PATH=/usr ../${P}
	)
	cmake_src_configure
}
