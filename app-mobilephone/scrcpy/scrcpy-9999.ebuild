# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg ninja-utils git-r3

PACKAGEAUTHOR="Genymobile"

EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"

if [[ ${PV} = 9999* ]]; then
	MY_SERVER_PV="3.1"
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/releases/download/v${MY_SERVER_PV}/${PN}-server-v${MY_SERVER_PV}"
else
	EGIT_COMMIT="v${PV}"
	MY_SERVER_PV="${PV}"
	KEYWORDS="~amd64 ~ppc64 ~x86"
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/releases/download/v${PV}/${PN}-server-v${PV}"
fi

DESCRIPTION="Display and control your Android device"
HOMEPAGE="https://blog.rom1v.com/2018/03/introducing-${PN}/
	https://github.com/${PACKAGEAUTHOR}/${PN}"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="lto"

RESTRICT="test"

DEPEND="
	media-libs/libsdl2[X]
	media-video/ffmpeg:=
	virtual/libusb:1
"
RDEPEND="
	${DEPEND}
	dev-util/android-tools
"
PDEPEND=""

src_configure() {
	local emesonargs=(
		$(meson_use lto b_lto)
			-Dprebuilt_server="${DISTDIR}/${PN}-server-v${MY_SERVER_PV}"
	)
	meson_src_configure
}

pkg_postinst() {
	xdg_pkg_postrm

	einfo "If you use pipewire because of a problem with libsdl2 it is possible that"
	einfo "scrcpy will not start, in which case start the program by exporting the"
	einfo "environment variable SDL_AUDIODRIVER=pipewire."
	einfo "For more information see https://github.com/Genymobile/scrcpy/issues/3864."
}
