# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit opam

PACKAGEAUTHOR="dbuenzli"

DESCRIPTION="Ocaml XML manipulation module"
HOMEPAGE="http://erratique.ch/software/${PN} https://github.com/${PACKAGEAUTHOR}/${PN}"

LICENSE="ISC"
SLOT="0/${PV}"
IUSE="test"
RESTRICT="!test? ( test )"

if [[ ${PV} = 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS="-amd64"
else
	SRC_URI="http://erratique.ch/software/${PN}/releases/${P}.tbz"
	KEYWORDS="~amd64"
fi

RDEPEND=""
DEPEND="${RDEPEND}
	>=dev-lang/ocaml-4.05.0:=
	>=dev-ml/findlib-1.2.7:=
	>=dev-ml/topkg-1.0.1:="

src_compile() {
	ocaml pkg/pkg.ml build \
		--tests "$(usex test 'true' 'false')" \
		|| die "compile failed"
}

src_test() {
	ocaml pkg/pkg.ml test || die "test failed"
}
