# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit dune

PACKAGEAUTHOR="astrada"

DESCRIPTION="OCaml binding for fuse"
HOMEPAGE="https://sourceforge.net/projects/${PN}/ https://${PACKAGEAUTHOR}.github.io/${PN}/ https://github.com/${PACKAGEAUTHOR}/${PN}"

LICENSE="GPL-2"
SLOT="0"
IUSE="ocamlopt"

if [[ ${PV} = 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS="-amd64 -x86"
else
	CVS=_cvs$(( ${PR#r} + 4))
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/archive/v${PV}${CVS}.tar.gz -> ${P}${CVS}.tar.gz"
	KEYWORDS="~amd64 ~x86"
	S="${WORKDIR}/${P}${CVS}"
fi

DEPEND="
	>=dev-lang/ocaml-4.02.3:=
	>=dev-ml/camlidl-1.05:=
	sys-fs/fuse:0
"
RDEPEND="${DEPEND}"

DOCS=( "README.md" )
