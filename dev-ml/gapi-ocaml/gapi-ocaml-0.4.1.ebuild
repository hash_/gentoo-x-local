# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit dune

PACKAGEAUTHOR="astrada"

DESCRIPTION="A simple OCaml client for Google Services"
HOMEPAGE="http://${PN}.forge.ocamlcore.org/ https://${PACKAGEAUTHOR}.github.io/${PN}/ https://github.com/${PACKAGEAUTHOR}/${PN}"

LICENSE="MIT"
SLOT="0"
IUSE="doc test ocamlopt"

if [[ ${PV} = 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PACKAGEAUTHOR}/${PN}.git"
	KEYWORDS="-amd64"
else
	SRC_URI="https://github.com/${PACKAGEAUTHOR}/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

RDEPEND="
	>=dev-lang/ocaml-4.02.3:=
	>=dev-ml/findlib-1.2.7:=
	>=dev-ml/ocamlnet-4.1.4:=
	>=dev-ml/ocurl-0.5.3:=
	>=dev-ml/cryptokit-1.3.14:=
	>=dev-ml/extlib-1.5.1:=
	>=dev-ml/yojson-1.0.2:=
	>=dev-ml/xmlm-1.0.2:=
"

DEPEND="${RDEPEND}
	test? (
		>=dev-ml/ounit-1.1.0
	)
"

DOCS=( "README.md" )
