# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

VALA_MIN_API_VERSION=0.56
VALA_MAX_API_VERSION=${VALA_MIN_API_VERSION}
VALA_USE_DEPEND="vapigen"

inherit vala

DESCRIPTION="Vala bindings for the Xfce desktop environment"
HOMEPAGE="https://wiki.xfce.org/vala-bindings"
SRC_URI="https://archive.xfce.org/src/bindings/${PN}/${PV%.*}/${P}.tar.bz2"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=xfce-base/exo-4.16:=
	>=xfce-base/garcon-4.16:=
	>=xfce-base/libxfce4ui-4.16:=[introspection]
	>=xfce-base/libxfce4util-4.16:=[introspection]
	>=xfce-base/xfce4-panel-4.16:=[introspection]
	>=xfce-base/xfconf-4.16:=
"

DEPEND="
	${RDEPEND}
	virtual/pkgconfig
"

BDEPEND="
	$(vala_depend)
"

src_prepare() {
	mv tests/exo-1.vala tests/exo-2.vala

	eapply "${FILESDIR}/xfce4-vala-4.10.3_001_configure.patch"

	mv packages/exo-1 packages/exo-2
	mv packages/exo-2/exo-1.namespace packages/exo-2/exo-2.namespace
	mv packages/exo-2/exo-1.metadata packages/exo-2/exo-2.metadata
	mv packages/exo-2/exo-1.gi packages/exo-2/exo-2.gi
	mv packages/exo-2/exo-1.files packages/exo-2/exo-2.files
	mv packages/exo-2/exo-1.excludes packages/exo-2/exo-2.excludes
	mv packages/exo-2/exo-1.deps packages/exo-2/exo-2.deps
	mv packages/exo-2/exo-1.defines packages/exo-2/exo-2.defines
	mv packages/exo-2/exo-1-custom.vala packages/exo-2/exo-2-custom.vala
	eapply "${FILESDIR}/xfce4-vala-4.10.3_002_exo-2.patch"

	mv packages/libxfce4ui-1 packages/libxfce4ui-2
	mv packages/libxfce4ui-2/libxfce4ui-1.namespace packages/libxfce4ui-2/libxfce4ui-2.namespace
	mv packages/libxfce4ui-2/libxfce4ui-1.metadata packages/libxfce4ui-2/libxfce4ui-2.metadata
	mv packages/libxfce4ui-2/libxfce4ui-1.gi packages/libxfce4ui-2/libxfce4ui-2.gi
	mv packages/libxfce4ui-2/libxfce4ui-1.files packages/libxfce4ui-2/libxfce4ui-2.files
	mv packages/libxfce4ui-2/libxfce4ui-1.excludes packages/libxfce4ui-2/libxfce4ui-2.excludes
	mv packages/libxfce4ui-2/libxfce4ui-1.deps packages/libxfce4ui-2/libxfce4ui-2.deps
	mv packages/libxfce4ui-2/libxfce4ui-1.defines packages/libxfce4ui-2/libxfce4ui-2.defines
	mv packages/libxfce4ui-2/libxfce4ui-1-custom.vala packages/libxfce4ui-2/libxfce4ui-2-custom.vala
	eapply "${FILESDIR}/xfce4-vala-4.10.3_003_libxfce4ui-2.patch"

	mv packages/libxfce4panel-1.0 packages/libxfce4panel-2.0
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.namespace packages/libxfce4panel-2.0/libxfce4panel-2.0.namespace
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.metadata packages/libxfce4panel-2.0/libxfce4panel-2.0.metadata
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.gi packages/libxfce4panel-2.0/libxfce4panel-2.0.gi
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.files packages/libxfce4panel-2.0/libxfce4panel-2.0.files
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.excludes packages/libxfce4panel-2.0/libxfce4panel-2.0.excludes
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.deps packages/libxfce4panel-2.0/libxfce4panel-2.0.deps
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0.defines packages/libxfce4panel-2.0/libxfce4panel-2.0.defines
	mv packages/libxfce4panel-2.0/libxfce4panel-1.0-custom.vala packages/libxfce4panel-2.0/libxfce4panel-2.0-custom.vala
	eapply "${FILESDIR}/xfce4-vala-4.10.3_004_libxfce4panel-2.0.patch"

	mv vapi/exo-1.deps vapi/exo-2.deps
	mv vapi/exo-1.vapi vapi/exo-2.vapi
	eapply "${FILESDIR}/xfce4-vala-4.10.3_005_exo-2.vapi.patch"

	mv vapi/libxfce4ui-1.deps vapi/libxfce4ui-2.deps
	mv vapi/libxfce4ui-1.vapi vapi/libxfce4ui-2.vapi
	eapply "${FILESDIR}/xfce4-vala-4.10.3_006_libxfce4ui-2.vapi.patch"

	mv vapi/libxfce4panel-1.0.deps vapi/libxfce4panel-2.0.deps
	mv vapi/libxfce4panel-1.0.vapi vapi/libxfce4panel-2.0.vapi
	eapply "${FILESDIR}/xfce4-vala-4.10.3_007_libxfce4panel-2.0.vapi.patch"

	default
}

src_configure() {
	local myconf=(
		"--with-vala-api=${VALA_MIN_API_VERSION}"
	)

	vala_setup --vala-api-version=${VALA_MIN_API_VERSION}
	econf "${myconf[@]}"
}
