# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8

inherit autotools xdg-utils

PACKAGEAUTHOR=nsz32
DESCRIPTION="A Dock-like Taskbar Plugin for XFCE"
HOMEPAGE="https://docs.xfce.org/panel-plugins/xfce4-docklike-plugin/start
	https://github.com/${PACKAGEAUTHOR}/${PN#xfce4-}"
PVP="$(ver_cut 1-2)"

if [[ ${PV} == *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="
		https://github.com/${PACKAGEAUTHOR}/${PN#xfce4-}
	"
	SRC_URI=""
else
	KEYWORDS="~amd64 ~x86"
	SRC_URI="
		https://archive.xfce.org/src/panel-plugins/${PN}/${PVP}/${PN}-${PV}.tar.bz2 -> ${P}.tar.bz2
	"
fi


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="debug"

RDEPEND="
	x11-libs/gtk+:3
	x11-libs/libX11
	x11-libs/libwnck:3
	>=x11-libs/cairo-1.16
	>=xfce-base/libxfce4ui-4.16:=
	>=xfce-base/libxfce4util-4.16:=
	>=xfce-base/xfce4-panel-4.16:=
	>=xfce-base/xfconf-4.14:=
"
DEPEND="${RDEPEND}
	dev-util/intltool
	dev-build/xfce4-dev-tools
	sys-devel/gettext
	virtual/pkgconfig
"

#src_prepare() {
#	default
#	./autogen.sh || die "autogen failed"
#	eautoconf
#}

src_prepare() {
	# run xdt-autogen from xfce4-dev-tools added as dependency by EAUTORECONF=1 to
	# rename configure.ac.in to configure.ac while grabbing $LINGUAS and $REVISION values
	NOCONFIGURE=1 xdt-autogen || die

	default
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
